# README

Rakuten test made with **React**,**Redux**,**Saga**,**Webpack** and **Axios**

## USAGE

Do **npm i** or **npm install** to download all dependencies and libraries of the project. 

- axios
- redux
- react-redux
- redux-saga
- styled-components
- react-router-dom
- owl-carousel
- prop-types
- jest

## Start Project

In terminal **npm run start** to run the project

## Project Structure

├── public                  # Folder with index.html file
├── src                     # Source files 
    ├── components             # Folder with all components
    ├── constants              # Folder with all constants
    ├── globalStyles           # Themes
    ├── hooks                  # Folder with hooks for the app
        ├──windowDimensions.js     # Hook for get the width and height dinamically of the browser
    ├── pages                  # Views of the app
        ├──App.js                  # Main file for the app an Provider store for Redux and theme
        ├──AppStyled.js            # Body Style
        ├──routes.js               # Routes for pages
    ├── redux                  # Folder with all files for redux
        ├──list                    # All redux files for list and sagas
        ├──movie                   # All redux files for movie and sagas
        ├──streaming               # All redux files for streaming and sagas  
        ├──actions.js                # General action 
        ├──reducer.js                # General reducer
        ├──types.js                  # General types
        ├──sagas.js                  # Root for Sagas
        ├──store.js                  # Store for Redux
    ├── services               # Folder with calls to api 
    ├── index.css              # Style for index.js
    ├── index.js               # File for render App.js file
    ├── setupTest.js           # Import for test



## Project made by Juan Ortega

[Linkedin Profile](https://www.linkedin.com/in/juan-ortega-066400151/)