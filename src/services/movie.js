import axios from "axios";
import { baseURL, queryParams } from "../constants/api";

const movieURL = "movies/";

export const getMovie = payload => {
  const id = payload.payload;
  const apiURL = baseURL + movieURL + id + queryParams;
  return axios
    .get(apiURL, { headers: { "Access-Control-Allow-Origin": "*" } })
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log(error);
      return error;
    });
};
