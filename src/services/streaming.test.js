import axios from "axios";
import { getTrailer } from "./streaming.js";

jest.mock("axios");
describe("getTrailer", () => {
  it("fetches successfully getTrailer", async () => {
    const expectedValue = "test";
    const response = {
      data: {
        data: "test"
      }
    };
    axios.post.mockImplementation(() => Promise.resolve(response));
    getTrailer({payload:"test"}).then(res => {
      expect(res).toMatchObject(expectedValue);
    });
  });
});