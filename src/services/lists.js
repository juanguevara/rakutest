import axios from "axios";
import { lists } from "../constants/list";

const baseURL = "https://gizmo.rakuten.tv/v3/lists/";
const queryParams =
  "?classification_id=5&device_identifier=web&locale=es&market_code=es";

const fetchData = URL => {
  return axios
    .get(URL, { headers: { "Access-Control-Allow-Origin": "*" } })
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log(error);
      return error
    });
};

export const getAllList = () => {
  const listOfApiURL = [];
  lists.map(list => {
    const apiURL = baseURL + list + queryParams;
    listOfApiURL.push(apiURL);
  });

  return Promise.all(listOfApiURL.map(fetchData));
};
