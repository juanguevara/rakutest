import axios from "axios";
import { baseURL, queryParams } from "../constants/api";

const streamingURL = "me/streamings";
const streamingBody = {
  audio_language: "SPA",
  audio_quality: "2.0",
  content_type: "movies",
  device_serial: "device_serial_1",
  device_stream_video_quality: "FHD",
  player: "web:PD-NONE",
  subtitle_language: "MIS",
  video_type: "trailer"
};

export const getTrailer = payload => {
  const id = payload.payload;
  const apiURL = baseURL + streamingURL + queryParams;

  return axios
    .post(apiURL, {
      ...streamingBody,
      content_id: id
    })
    .then(response => {
      return response.data.data;
    })
    .catch(error => {
      console.log(error);
      return error;
    });
};
