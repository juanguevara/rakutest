import axios from "axios";
import { getAllList } from "./lists.js";

jest.mock("axios");
describe("getAllList", () => {
  it("fetches successfully AllList", async () => {
    const expectedValue = [
      "test",
      "test",
      "test",
      "test",
      "test",
      "test",
      "test"
    ];
    const response = {
      data: {
        data: "test"
      }
    };
    axios.get.mockImplementation(() => Promise.resolve(response));
    getAllList().then(response => {
      expect(response).toMatchObject(expectedValue);
    });
  });
});
