import axios from "axios";
import { getMovie } from "./movie.js";

jest.mock("axios");
describe("getMovie", () => {
  it("fetches successfully getMovie", async () => {
    const expectedValue = "test";
    const response = {
      data: {
        data: "test"
      }
    };
    axios.get.mockImplementation(() => Promise.resolve(response));
    getMovie({payload:"test"}).then(res => {
      expect(res).toMatchObject(expectedValue);
    });
  });
});