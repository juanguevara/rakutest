import styled from "styled-components";

export const ArtworkContainer = styled.div`
  display: flex;
  flex-direction: row;
  overflow-x: auto;
  overflow-y: hidden;

  ::-webkit-scrollbar {
    display: none;
  }
`;

export const ArtworkImgContainer = styled.div`
  width: 150px;
  height: 200px;
`;

export const ArtworkImg = styled.div`
  height: 100%;
  width: 150px;
  display: block;
  position: relative;
  background-repeat: no-repeat;
  background-position: center center;
  background-size: cover;
  border-radius: 2px;
  cursor:pointer;

  img {
    display: block;
    position: absolute;
    top: 0;
    width: 100%;
    border-radius: 2px;
    padding: 5px;
  }
`;
