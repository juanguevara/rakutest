import React from "react";
import PropTypes from "prop-types";
import { useHistory } from "react-router-dom";

import {
  ArtworkContainer,
  ArtworkImgContainer,
  ArtworkImg
} from "./ArtworkStyled";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import "../../globalStyles/owl-carousel.css";

const Artwork = ({ movies }) => {
  const history = useHistory();

  const classButtons = ["owl-prev", "owl-next"];

  const redirectToMovieDetail = id => {
    history.push(`/movie/${id}`);
  };

  return (
    <ArtworkContainer>
      <OwlCarousel
        className="owl-theme"
        dots={false}
        items={3}
        autoWidth
        margin={10}
        nav
        navClass={classButtons}
      >
        {movies.map((movie, key) => {
          return (
            <ArtworkImgContainer key={key}>
              <ArtworkImg onClick={() => redirectToMovieDetail(movie.id)}>
                <img src={movie.images.artwork} />
              </ArtworkImg>
            </ArtworkImgContainer>
          );
        })}
      </OwlCarousel>
    </ArtworkContainer>
  );
};

Artwork.propTypes = {
  movies: PropTypes.array.isRequired
};

export default Artwork;
