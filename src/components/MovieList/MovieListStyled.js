import styled from "styled-components";

export const MovieListTitle = styled.h3`${({ theme }) => `
  color: ${theme.white};
  font-size: 15px;
`}
`;

export const MovieListContainer = styled.div`
  display: flex;
  flex-direction: column;
  overflow-x: hidden;
  height: 100%;
  margin: 20px 0;
`;