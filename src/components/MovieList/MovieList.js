import React from "react";
import PropTypes from "prop-types";

import { MovieListTitle, MovieListContainer } from "./MovieListStyled";
import Artwork from "../Artwork";

const MovieList = ({ lists }) => {
  return lists.map((list, key) => {
    return (
      <MovieListContainer key={key}>
        <MovieListTitle>{list.name}</MovieListTitle>
        <Artwork movies={list.contents.data} />
      </MovieListContainer>
    );
  });
};

MovieList.propTypes = {
  lists: PropTypes.array.isRequired
};

export default MovieList;
