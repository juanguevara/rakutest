import React from "react";
import Navbar from "./Navbar";
import { render } from "@testing-library/react";
import { useSelector } from "react-redux";

jest.mock("react-redux");
describe("NavBar", () => {

  it("Should render snapshot", () => {
    const { container } = render(<Navbar />);
    useSelector.mockImplementationOnce(() => null);
    expect(container).toMatchSnapshot();
  });
});
