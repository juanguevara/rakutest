import styled from "styled-components";

export const NavbarContainer = styled.nav`
  ${({ theme }) => `
  padding: 10px 15px;
  background: ${theme.black};
  color: ${theme.white};
  height: 50px;
  display: flex;
  align-items:center;
`}
`;

export const NavbarTitle = styled.h1`
  font-size: 20px;
  margin: 0;
  width: 100%;
  text-align: left;
  cursor:pointer;
`;
