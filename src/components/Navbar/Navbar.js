import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { NavbarContainer, NavbarTitle } from "./NavbarStyled";
import { movieDetailSelector } from "../../redux/movie/selectors";

const Navbar = () => {
  const history = useHistory();
  const movie = useSelector(movieDetailSelector);

  const redirectToHome = () => {
    history.push("/");
  };
  return (
    <NavbarContainer>
      <NavbarTitle onClick={() => redirectToHome()}>
        {movie ? movie.title : "Rakuten TV"}
      </NavbarTitle>
    </NavbarContainer>
  );
};

export default Navbar;
