export default {
    black:"#000",
    white:"#FFF",
    yellow:"#ffcc00",
    green:"#b8d916",
    orange:"#cc3300",
    mainBackground:"#171717",
    textColor:"#252525",
}