import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { getListStart } from "../../redux/list/actions";
import { cleanState } from "../../redux/actions";
import { typeOfListSelector } from "../../redux/list/selectors";

import MovieList from "../../components/MovieList";

const Home = () => {
  const dispatch = useDispatch();
  const lists = useSelector(typeOfListSelector);

  useEffect(() => {
    dispatch(cleanState());
    dispatch(getListStart());
  }, []);

  return <>{!lists.length ? "Cargando" : <MovieList lists={lists} />}</>;
};

export default Home;
