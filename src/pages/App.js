import React from "react";
import { ThemeProvider } from "styled-components";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "../redux/store";

import theme from "../globalStyles/theme";
import { GlobalStyle } from "./AppStyled";

import * as Routes from "./routes";

import Navbar from "../components/Navbar";
import Home from "./Home";
import Movie from "./Movie";
import Streaming from "./Streaming";
import Error from "./Error"

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle theme={theme} />
      <Provider store={store}>
        <Router>
          <Navbar />
          <Switch>
            <Route path={Routes.HOME} exact component={Home} />
            <Route path={Routes.MOVIE} exact component={Movie} />
            <Route path={Routes.STREAMING} exact component={Streaming} />
            <Route component={Error} />
          </Switch>
        </Router>
      </Provider>
    </ThemeProvider>
  );
};

export default App;
