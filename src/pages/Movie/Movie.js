import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import "../../globalStyles/owl-carousel.css"

import {
  MovieContainer,
  MovieArtwork,
  MovieDetailContentContainer,
  BlockDetailContainer,
  InlineBlockDetailContainer,
  BlockDetail,
  BlockDetailLabel,
  WatchTrailer,
  MovieArtworkContainer,
  MovieTitleContainer,
  MovieTitle,
  MovieScore,
  ActorContainer,
  ActorPortrait,
  ActorName,
} from "./MovieStyled";
import {
  movieDetailSelector,
  errorMovieSelector
} from "../../redux/movie/selectors";
import { getMovieStart } from "../../redux/movie/actions";
import theme from "../../globalStyles/theme";
import useWindowDimensions from "../../hooks/windowDimensions";

const Movie = () => {
  const movie = useSelector(movieDetailSelector);
  const error = useSelector(errorMovieSelector);
  const dispatch = useDispatch();
  const history = useHistory();
  const { id } = useParams();
  const yellow = theme.yellow;

  const { width } = useWindowDimensions();

  const classButtons = ["owl-prev", "owl-next"];

  if (error) {
    history.push("/error");
  }
  useEffect(() => {
    dispatch(getMovieStart(id));
  }, []);

  const redirectToTrailer = id => {
    history.push(`/streaming/${id}`);
  };
  return (
    <MovieContainer>
      {!movie ? (
        "Cargando"
      ) : (
        <>
          <MovieArtworkContainer>
            <MovieArtwork
              src={width > 767 ? movie.images.snapshot : movie.images.artwork}
              alt={`artwork-${id}`}
            />
            <WatchTrailer onClick={() => redirectToTrailer(id)}>
              Ver Trailer
            </WatchTrailer>
            <MovieTitleContainer>
              {width > 767 ? (
                <>
                  <MovieScore>
                    <span>{movie.scores[0].site.name}</span>
                    <span>{movie.scores[0].site.scale}</span>
                  </MovieScore>
                  <MovieTitle>{movie.title}</MovieTitle>
                </>
              ) : null}
            </MovieTitleContainer>
          </MovieArtworkContainer>
          <MovieDetailContentContainer>
            <BlockDetailContainer>
              <BlockDetail>Edad:{" " + movie.classification.name}</BlockDetail>
              <BlockDetail>{movie.duration + " "}minutos</BlockDetail>
              <BlockDetail>{movie.year}</BlockDetail>
              <BlockDetail>{movie.countries[0].name}</BlockDetail>
              <BlockDetail>
                Título original:{" " + movie.original_title}
              </BlockDetail>
            </BlockDetailContainer>
            <BlockDetailContainer>
              <BlockDetailLabel background={yellow}>
                {movie.labels.hdr_types[0].name}
              </BlockDetailLabel>
            </BlockDetailContainer>
            <BlockDetailContainer>
              <BlockDetail plot>{movie.plot}</BlockDetail>
            </BlockDetailContainer>
            <BlockDetailContainer>
              <BlockDetail bold title>
                Dirección y reparto
              </BlockDetail>
              <BlockDetail fullWidth>
                <OwlCarousel
                  className="owl-theme"
                  dots={false}
                  items={3}
                  autoWidth
                  margin={10}
                  nav
                  navClass={classButtons}
                >
                  {movie.actors.map((actor, key) => {
                    return (
                      <ActorContainer key={key}>
                        <ActorPortrait portrait={actor.photo} />
                        <ActorName>{actor.name}</ActorName>
                      </ActorContainer>
                    );
                  })}
                </OwlCarousel>
              </BlockDetail>
            </BlockDetailContainer>
            <InlineBlockDetailContainer center>
              <BlockDetail bold title block>
                Puntuación
              </BlockDetail>
              {movie.scores.map((score, key) => {
                return (
                  <BlockDetail score={score.site.scale} bold center key={key}>
                    <span>{score.site.scale}</span>
                    <br />
                    {score.site.name}
                  </BlockDetail>
                );
              })}
            </InlineBlockDetailContainer>
            <InlineBlockDetailContainer center>
              <BlockDetail bold title block>
                Géneros
              </BlockDetail>
              {movie.genres.map((genre, key) => {
                return (
                  <BlockDetail bold key={key}>
                    {genre.name}
                  </BlockDetail>
                );
              })}
            </InlineBlockDetailContainer>
          </MovieDetailContentContainer>
        </>
      )}
    </MovieContainer>
  );
};

export default Movie;
