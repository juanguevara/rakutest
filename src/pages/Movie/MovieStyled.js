import styled from "styled-components";

export const MovieContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const MovieArtworkContainer = styled.div`
  height: calc(100vh - 50px);
  width: 100%;
  position: relative;
`;

export const MovieArtwork = styled.img`
  width: 100%;
  height: 100%;
`;

export const MovieDetailContentContainer = styled.div`
  ${({ theme }) => `
    position: relative;
    padding: 15px 0 25px;
    background-color: ${theme.white};
    z-index: 0;
`}
`;

export const BlockDetailContainer = styled.div`
  margin-bottom: 10px;
  color: ${props => props.theme.textColor};
  padding: 0 15px;
  text-align: ${props => props.center && "center"};
`;

export const InlineBlockDetailContainer = styled(BlockDetailContainer)`
  display: inline-block;
`;

export const BlockDetail = styled.div`
  display: ${props => (props.block ? "block" : "inline-block")};
  width:${props => props.fullWidth && "100%"};
  margin-right: 15px;
  margin-bottom: ${props => (props.title ? "15px" : "10px")};
  font-family: sans-serif;
  font-size: ${props => (props.plot ? "16px" : props.title ? "18px" : "14px")};
  line-height: ${props => props.plot && "180%"};
  text-align:${props =>
    props.plot ? "justify" : props.center ? "center" : "initial"};
  font-weight:${props => props.bold && "bold"};
  position:relative;
  

  span {
    font-size: 20px;
    color:${props =>
      props.score >= 5 ? props.theme.green : props.theme.orange};
  }
}

`;

export const BlockDetailLabel = styled.span`
  display: inline-block;
  background-color: ${props => props.background};
  color: ${props => props.theme.black};
  float: left;
  margin-bottom: 15px;
  padding: 2px;
`;

export const WatchTrailer = styled.span`
  ${({ theme }) => `
  font-weight: bold;
    position: absolute;
    bottom: 30px;
    right: 15px;
    cursor: pointer;
    font-size: 22px;
    background: ${theme.yellow};
    color: ${theme.black};
    padding: 8px 10px;

  &:hover {
    background: ${theme.black};
    color: ${theme.yellow}; 
  }
`}
`;

export const MovieTitleContainer = styled.div`
  position: absolute;
  top: 70%;
  width: 100%;
  letter-spacing: 0.5px;
  text-align: center;
  font-weight: bold;
`;

export const MovieTitle = styled.div`
  ${({ theme }) => `
    font-size: 36px;
    text-shadow: 2px 2px 2px ${theme.black};
`}
`;

export const MovieScore = styled.div`
  ${({ theme }) => `
  font-size: 25px;
  text-shadow: 2px 2px 2px ${theme.black};

  span {
    &:first-child {
      background:${theme.yellow}
    }

    &:last-child {
      color: ${theme.yellow};
    }
}

`}
`;

export const ActorContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 150px;
  align-items: center;
  justify-content: center;
  padding: 5px;
`;

export const ActorPortrait = styled.div`
  background: url(${props => props.portrait});
  width: 100%;
  height: 150px;
  background-size: cover;
  background-repeat: no-repeat;
`;

export const ActorName = styled.p`
  font-size: 15px;
  font-weight: bold;
  width: 100%;
  margin: 5px;
`;
