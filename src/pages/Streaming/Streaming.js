import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ReactPlayer from "react-player";
import { useParams, useHistory } from "react-router-dom";

import { TrailerContainer } from "./StreamingStyled";
import {
  trailerSelector,
  errorTrailerSelector
} from "../../redux/streaming/selectors";
import { getStreamingStart } from "../../redux/streaming/actions";

const Streaming = () => {
  const trailer = useSelector(trailerSelector);
  const error = useSelector(errorTrailerSelector);
  const { id } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  ;
  if (error) {
    history.push("/error");
  }

  useEffect(() => {
    dispatch(getStreamingStart(id));
  }, []);
  return (
    <TrailerContainer>
      {!trailer ? (
        "Cargando"
      ) : (
        <ReactPlayer
          url={trailer.stream_infos[0].url}
          width="100%"
          height="100%"
          playing
          light
          controls
        />
      )}
    </TrailerContainer>
  );
};

export default Streaming;
