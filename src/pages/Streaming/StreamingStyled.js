import styled from "styled-components";

export const TrailerContainer = styled.div`
  ${({ theme }) => `
  background-color: ${theme.mainBackground};
  position: absolute;
  transform: translateY(50%);
  bottom: 50%;
  left:0;
  right:0;
  top:1px;
`}
`;
