import { combineReducers } from "redux";
import listReducer from "./list/reducer";
import movieReducer from "./movie/reducer";
import streamingReducer from "./streaming/reducer";

export default combineReducers({
  list: listReducer,
  movie: movieReducer,
  streaming: streamingReducer
});
