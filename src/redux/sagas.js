import { takeLatest, all } from "redux-saga/effects";

import * as listTypes from "./../redux/list/types";
import * as movieTypes from "./../redux/movie/types";
import * as streamingTypes from "./../redux/streaming/types";

import getLists from "./../redux/list/sagas/get-list-saga";
import getMovie from "./../redux/movie/sagas/get-movie-saga";
import getTrailer from "./../redux/streaming/sagas/get-streaming-saga"

export default function* rootSaga() {
  yield all([
    takeLatest(listTypes.GET_LIST_START, getLists),
    takeLatest(movieTypes.MOVIE_DETAILS_START, getMovie),
    takeLatest(streamingTypes.STREAMING_START,getTrailer)
  ]);
}
