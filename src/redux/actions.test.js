import { cleanState } from "./actions";
import * as Types from "./Types";

describe("Clean state actions", () => {
  it("should cleanState", () => {
    const expectedValue = cleanState();
    expect(expectedValue).toMatchObject({
      type: Types.CLEAN_STATES
    });
  });
});
