import { getTrailer } from "../../../services/streaming";
import { getStreamingSuccess, getStreamingError } from "../actions";
import { call, put } from "redux-saga/effects";

export default function* get(payload) {
  try {
    const res = yield call(getTrailer, payload);
    
    yield put(getStreamingSuccess(res));
  } catch (error) {
    
    yield put(getStreamingError(error));
  }
}
