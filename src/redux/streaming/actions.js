import * as Types from "./Types";

export const getStreamingStart = id => ({
  type: Types.STREAMING_START,
  payload: id
});


export const getStreamingSuccess = payload => ({
  type: Types.STREAMING_SUCCESS,
  payload: payload
});

export const getStreamingError = error => ({
  type: Types.STREAMING_ERROR,
  payload: error,
});
