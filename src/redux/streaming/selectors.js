export const streamingStateSelector = state => state.streaming;

export const  trailerSelector = state => streamingStateSelector(state).trailer;

export const  errorTrailerSelector = state => streamingStateSelector(state).error;

