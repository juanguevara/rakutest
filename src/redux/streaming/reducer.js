import * as Types from "./Types";
import { CLEAN_STATES } from "../types";

export const initialState = {
  error: null,
  loading: false,
  trailer:null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.STREAMING_START:
      return {
        ...state,
        loading: true
      };
    case Types.STREAMING_SUCCESS:
      return {
        ...state,
        loading: false,
        trailer: action.payload
      };
    case Types.STREAMING_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    case CLEAN_STATES:
      return {
        ...state,
        trailer:null
      }
    default:
      return state;
  }
};
