import {
  getStreamingStart,
  getStreamingError,
  getStreamingSuccess
} from "./actions";
import * as Types from "./Types";

describe("streaming actions", () => {
  it("should getStreamingStart", () => {
    const expectedValue = getStreamingStart("test");
    expect(expectedValue).toMatchObject({
      type: Types.STREAMING_START,
      payload: "test"
    });
  });

  it("should getStreamingSuccess", () => {
    const expectedValue = getStreamingSuccess("success");
    expect(expectedValue).toMatchObject({
      type: Types.STREAMING_SUCCESS,
      payload: "success"
    });
  });

  it("should getStreamingError", () => {
    const expectedValue = getStreamingError("error");
    expect(expectedValue).toMatchObject({
      type: Types.STREAMING_ERROR,
      payload: "error"
    });
  });
});
