export const listsSelector = state => state.list;

export const typeOfListSelector = state => listsSelector(state).listState;
