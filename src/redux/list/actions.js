import * as Types from "./Types";

export const getListStart = () => ({
  type: Types.GET_LIST_START,
});


export const getListSuccess = payload => ({
  type: Types.GET_LIST_SUCCESS,
  payload: payload
});

export const getListError = error => ({
  type: Types.GET_LIST_ERROR,
  payload: error,
});
