import { getAllList } from "../../../services/lists";
import { getListSuccess, getListError } from "../actions";
import { call, put } from "redux-saga/effects";

export default function* get() {
  try {
    const res = yield call(getAllList);

    yield put(getListSuccess(res));
  } catch (error) {
    
    yield put(getListError(error));
  }
}
