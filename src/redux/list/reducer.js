import * as Types from "./Types";

export const initialState = {
  error: null,
  loading: false,
  listState: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.GET_LIST_START:
      return {
        ...state,
        loading: true
      };
    case Types.GET_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        listState: action.payload
      };
    case Types.GET_LIST_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    default:
      return state;
  }
};
