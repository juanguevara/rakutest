import { getListStart, getListSuccess, getListError } from "./actions";
import * as Types from "./Types";

describe("list actions", () => {
  it("should getListStart", () => {
    const expectedValue = getListStart();
    expect(expectedValue).toMatchObject({
      type: Types.GET_LIST_START,
    });
  });

  it("should getListSuccess", () => {
    const expectedValue = getListSuccess("success");
    expect(expectedValue).toMatchObject({
      type: Types.GET_LIST_SUCCESS,
      payload: "success"
    });
  });

  it("should getListError", () => {
    const expectedValue = getListError("error");
    expect(expectedValue).toMatchObject({
      type: Types.GET_LIST_ERROR,
      payload: "error"
    });
  });
});
