import { CLEAN_STATES } from "./types";

export const cleanState = () => ({
  type: CLEAN_STATES
});
