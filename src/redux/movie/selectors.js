export const movieStateSelector = state => state.movie;

export const movieDetailSelector = state => movieStateSelector(state).movieDetails;

export const errorMovieSelector = state => movieStateSelector(state).error;