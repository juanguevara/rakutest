import * as Types from "./Types";
import { CLEAN_STATES } from "../types";

export const initialState = {
  error: null,
  loading: false,
  movieDetails: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.MOVIE_DETAILS_START:
      return {
        ...state,
        loading: true
      };
    case Types.MOVIE_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        movieDetails: action.payload
      };
    case Types.MOVIE_DETAILS_ERROR:
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    case CLEAN_STATES:
      return {
        ...state,
        movieDetails: null
      }
    default:
      return state;
  }
};
