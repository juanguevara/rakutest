import * as Types from "./Types";

export const getMovieStart = id => ({
  type: Types.MOVIE_DETAILS_START,
  payload: id
});


export const getMovieSuccess = payload => ({
  type: Types.MOVIE_DETAILS_SUCCESS,
  payload: payload
});

export const getMovieError = error => ({
  type: Types.MOVIE_DETAILS_ERROR,
  payload: error,
});
