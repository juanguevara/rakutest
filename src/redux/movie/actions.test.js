import { getMovieStart, getMovieSuccess, getMovieError } from "./actions";
import * as Types from "./Types";

describe("movie actions", () => {
  it("should getMovieStart", () => {
    const expectedValue = getMovieStart("test");
    expect(expectedValue).toMatchObject({
      type: Types.MOVIE_DETAILS_START,
      payload: "test"
    });
  });

  it("should getMovieSuccess", () => {
    const expectedValue = getMovieSuccess("success");
    expect(expectedValue).toMatchObject({
      type: Types.MOVIE_DETAILS_SUCCESS,
      payload: "success"
    });
  });

  it("should getMovieError", () => {
    const expectedValue = getMovieError("error");
    expect(expectedValue).toMatchObject({
      type: Types.MOVIE_DETAILS_ERROR,
      payload: "error"
    });
  });
});
