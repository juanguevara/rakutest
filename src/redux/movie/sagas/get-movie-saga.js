import { getMovie } from "../../../services/movie";
import { getMovieSuccess, getMovieError } from "../actions";
import { call, put } from "redux-saga/effects";

export default function* get(payload) {
  try {
    const res = yield call(getMovie, payload);
    
    yield put(getMovieSuccess(res));
  } catch (error) {
    
    yield put(getMovieError(error));
  }
}
